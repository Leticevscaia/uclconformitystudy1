%{
runThreewayAnova.m script downloads the dataset from the Excel file and
runs threeway ANOVA exploring the all possible interactions between levels as shown here:
https://uk.mathworks.com/help/stats/anovan.html

author: Olga Leticevscaia
  %}

df = readtable('anovaDataset.xlsx');
categoryArray = table2array(df(:,2));
groupArray = table2array(df(:,3));
prefArray = table2array(df(:,4));
ratingArray = table2array(df(:,5));

p = anovan(ratingArray,{categoryArray groupArray prefArray},'model','interaction','varnames',{'categoryArray','groupArray','prefArray'})
